# free2pass_support

The support library providing shared codes of the different parts of the free2pass apps.

## Getting Started

This library will usually be used by the free2**pass** app developed by HannTech GmbH

All further information can be found in the [repository of the main app](https://gitlab.com/free2pass/free2pass-app/).