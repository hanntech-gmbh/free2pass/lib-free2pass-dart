import 'package:flutter/material.dart';
import 'package:free2pass_support/generated/l10n.dart';

class QrDecoration extends StatefulWidget {
  final Widget? child;
  final ValueChanged<bool>? onToggleFlash;
  final VoidCallback? onAbort;

  const QrDecoration({Key? key, this.child, this.onToggleFlash, this.onAbort})
      : super(key: key);

  @override
  _QrDecorationState createState() => _QrDecorationState();
}

class _QrDecorationState extends State<QrDecoration> {
  bool _flashEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        fit: StackFit.expand,
        children: [
          widget.child!,
          SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(8),
                  child: IconTheme.merge(
                    data: IconThemeData(color: Colors.white),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: widget.onAbort,
                          icon: Icon(Icons.close),
                          tooltip: S.of(context).close,
                        ),
                        widget.onToggleFlash == null
                            ? Container()
                            : IconButton(
                                onPressed: _toggleFlash,
                                icon: Icon(_flashEnabled
                                    ? Icons.flash_off
                                    : Icons.flash_on),
                                tooltip: S.of(context).toggleFlash,
                              )
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    ),
                  ),
                ),
                Center(
                  child: Card(
                    color: Theme.of(context).cardColor.withOpacity(.2),
                    margin: EdgeInsets.all(64),
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Text(S.of(context).lookingForQrCode),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future _toggleFlash() async {
    setState(() {
      _flashEnabled = !_flashEnabled;
    });
    widget.onToggleFlash!(_flashEnabled);
  }
}
