import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:free2pass_support/generated/l10n.dart';
import 'package:free2pass_support/src/dialogs/qr-scanner/qr-decoration.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

typedef ScanResultCallback(String data);

/// displays a camera view, a symbolic frame and handles scanned QR code data
class QrScannerWidget extends StatefulWidget {
  /// the [RegExp] to be matched against
  final RegExp? pattern;

  /// the [ScanResultCallback] to be triggered as soon as any matching data is found
  final ScanResultCallback? onData;

  /// what to do in case the "Close" button is pressed
  final VoidCallback? onAbort;

  const QrScannerWidget({Key? key, this.pattern, this.onData, this.onAbort})
      : super(key: key);
  @override
  _QrScannerWidgetState createState() => _QrScannerWidgetState();
}

class _QrScannerWidgetState extends State<QrScannerWidget> {
  Barcode? result;

  QRViewController? controller;

  final _qrViewKey = GlobalKey();

  bool _foundData = false;

  @override
  void reassemble() {
    super.reassemble();

    if (Theme.of(context).platform == TargetPlatform.android) {
      controller!.pauseCamera();
    } else if (Theme.of(context).platform == TargetPlatform.iOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return QrDecoration(
      child: QRView(
        key: _qrViewKey,
        onQRViewCreated: _onQRViewCreated,
        formatsAllowed: [BarcodeFormat.qrcode],
        overlay: QrScannerOverlayShape(
            borderColor: Theme.of(context).primaryColor,
            borderRadius: 16,
            borderLength: 30,
            borderWidth: 10,
            cutOutSize: _squaredAxis()),
      ),
      onAbort: widget.onAbort,
      onToggleFlash: kIsWeb ? null : _toggleFlash,
    );
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen(_onScanData);
  }

  _onScanData(Barcode scanData) async {
    if (!_foundData) {
      _foundData = true;
      if (widget.pattern!.hasMatch(scanData.code!)) {
        widget.onData!(scanData.code!);
      } else {
        final ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
            scaffoldFeatureController =
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).thatsNotAMatchingQrCode),
        ));
        scaffoldFeatureController.closed.then((value) => _foundData = false);
      }
    }
  }

  Future _toggleFlash(bool data) async {
    await controller!.toggleFlash();
  }

  double _squaredAxis() {
    if (MediaQuery.of(context).size.width > MediaQuery.of(context).size.height)
      return MediaQuery.of(context).size.height * 2 / 3;
    else
      return MediaQuery.of(context).size.width * 2 / 3;
  }
}

/// a Dialog for scanning QR codes with
/// if pops in history returning [Null] in case the user aborts the process
/// as soon as matching data is found, the data is being popped
class QrScannerDialog extends StatefulWidget {
  /// the [RegExp] pattern to match against
  final RegExp? pattern;

  const QrScannerDialog({Key? key, this.pattern}) : super(key: key);

  @override
  _QrScannerDialogState createState() => _QrScannerDialogState();
}

class _QrScannerDialogState extends State<QrScannerDialog> {
  @override
  Widget build(BuildContext context) {
    return QrScannerWidget(
      pattern: widget.pattern,
      onData: _onData,
      onAbort: _onAbort,
    );
  }

  _onData(String data) {
    Navigator.of(context).pop(data);
  }

  void _onAbort() {
    Navigator.of(context).pop();
  }
}
