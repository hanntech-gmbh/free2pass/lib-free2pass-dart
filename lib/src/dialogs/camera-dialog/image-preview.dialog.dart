import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:free2pass_support/generated/l10n.dart';

/// a dialog previewing an in image popping ether false or true
class ImagePreviewDialog extends StatelessWidget {
  /// the [FilePickerCross] image to be previewed
  final FilePickerCross image;

  /// the [BoxShape] to be applied to the image
  ///
  /// default to [BoxShape.circle]
  final BoxShape shape;

  const ImagePreviewDialog(
      {Key? key, required this.image, this.shape = BoxShape.circle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(S.of(context).areYourConfidentWithThePhoto),
      content: ClipRRect(
        child: AspectRatio(
          aspectRatio: 1,
          child: Container(
            decoration: BoxDecoration(
                shape: shape,
                image: DecorationImage(
                    image: MemoryImage(image.toUint8List()),
                    fit: BoxFit.cover)),
          ),
        ),
        borderRadius: BorderRadius.circular(16),
      ),
      actions: [
        TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(S.of(context).no)),
        TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text(S.of(context).yes)),
      ],
    );
  }
}
