import 'package:flutter/services.dart';

/// a [TextInputFormatter] formatting latin names to have uppercase letters at the beginning of each word
class NameCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String newText = newValue.text;
    if (newText.isNotEmpty) {
      final handle = RegExp(r'(^|\s)[a-z]');
      if (handle.hasMatch(newText)) {
        final matches = handle.allMatches(newText);
        newText = newText.replaceRange(
            matches.first.end - 1,
            matches.first.end,
            newText
                .substring(matches.first.end - 1, matches.first.end)
                .toUpperCase()
                .trim());
      }
    }
    return TextEditingValue(
      text: newText,
      selection: newValue.selection,
    );
  }
}
