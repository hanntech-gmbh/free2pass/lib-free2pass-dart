/// provides friendly and yummy placeholders
class PlaceholderGenerator {
  static const _baseText =
      'Cupcake ipsum dolor sit amet chocolate cake I love jujubes. Bear claw tiramisu topping croissant ice cream liquorice bear claw. I love I love chupa chups marzipan tart sweet fruitcake. Cupcake tiramisu bear claw ice cream jelly beans jelly-o chocolate jujubes. Cupcake pastry toffee I love. Icing danish brownie jelly bonbon toffee jelly beans.';

  /// a yummy placeholder text with the given number of [paragraphs]
  static cupcakeIpsum({int paragraphs = 1}) {
    String result = '';
    for (int i = 0; i < paragraphs; i++) {
      result += '\n' + _baseText;
    }
    return result.trim();
  }
}
