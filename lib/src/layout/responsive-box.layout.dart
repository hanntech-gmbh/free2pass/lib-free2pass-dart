import 'package:flutter/material.dart';

/// a simple Widget providing a maximum width to it's child
class ResponsiveBox extends StatelessWidget {
  /// the [Widget] to be rendered as child
  final Widget? child;

  const ResponsiveBox({Key? key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      child: Container(
        constraints: BoxConstraints(maxWidth: 786, minHeight: 256),
        width: double.maxFinite,
        child: child,
      ),
    );
  }
}
