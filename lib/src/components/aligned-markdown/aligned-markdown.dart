import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../free2pass_support.dart';

/// provides an interface to align each line in a [MarkdownBody]
///
/// please note that automatic line breaks are not handled
class AlignedMarkdown extends StatelessWidget {
  /// the raw markdown String to be rendered
  final String data;

  /// the [CrossAxisAlignment] used to align each line
  /// default to [CrossAxisAlignment.center]
  final CrossAxisAlignment alignment;

  /// the text color
  final Color? color;

  const AlignedMarkdown(
      {Key? key,
      required this.data,
      this.alignment = CrossAxisAlignment.center,
      this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<String> substrings = data.split('\n');
    substrings.removeWhere((element) => element.isEmpty);
    return Column(
      crossAxisAlignment: alignment,
      mainAxisSize: MainAxisSize.min,
      children: substrings
          .map((e) => MarkdownBody(
                data: e,
                onTapLink: _onTapLink,
                styleSheet: MarkdownStyleSheet(
                    p: TextStyle(color: color),
                    h1: TextStyle(color: color),
                    h2: TextStyle(color: color),
                    h3: TextStyle(color: color),
                    h4: TextStyle(color: color),
                    h5: TextStyle(color: color),
                    h6: TextStyle(color: color),
                    a: TextStyle(
                        decoration: TextDecoration.underline, color: blue)),
              ))
          .toList(),
    );
  }

  void _onTapLink(String text, String? href, String title) {
    if (href != null) launch(href);
  }
}
