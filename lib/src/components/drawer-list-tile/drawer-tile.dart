import 'package:flutter/material.dart';

/// a high-level implementation of a [ListTile] to be used in a [Drawer]
/// in case the [DrawerTile]'s [routeName] is currently active, the
/// tile is emphasized.
class DrawerTile extends StatefulWidget {
  /// the displayed label text
  final String label;

  /// the leading [IconData] to be implied
  final IconData? icon;

  /// the route name to be pushed on tap
  final String routeName;

  const DrawerTile(
      {Key? key, required this.label, this.icon, required this.routeName})
      : super(key: key);

  @override
  _DrawerTileState createState() => _DrawerTileState();
}

class _DrawerTileState extends State<DrawerTile> {
  @override
  Widget build(BuildContext context) {
    final tile = ListTile(
      leading: Icon(
        widget.icon,
        color: Colors.white,
      ),
      title: DefaultTextStyle(
        child: Text(widget.label),
        style: TextStyle(color: Colors.white),
      ),
      onTap: _navigate,
    );
    if (ModalRoute.of(context)!.settings.name == widget.routeName) {
      return Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Container(
              height: 48,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(.25),
                borderRadius: BorderRadius.circular(8),
              ),
            ),
          ),
          tile,
        ],
      );
    } else
      return tile;
  }

  void _navigate() {
    if (widget.routeName.isEmpty ||
        ModalRoute.of(context)!.settings.name == widget.routeName)
      Navigator.of(context).pop();
    else {
      Navigator.of(context).pop();
      Navigator.of(context).pushNamed(widget.routeName);
    }
  }
}
