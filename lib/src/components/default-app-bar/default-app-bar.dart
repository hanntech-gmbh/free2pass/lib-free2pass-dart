import 'package:flutter/material.dart';

import '../../../free2pass_support.dart';

/// the default [AppBar] displayed on almost every page of our apps
class DefaultAppBar extends StatefulWidget implements PreferredSizeWidget {
  final List<Widget>? actions;

  const DefaultAppBar({Key? key, this.actions}) : super(key: key);
  @override
  _DefaultAppBarState createState() => _DefaultAppBarState();

  @override
  Size get preferredSize =>
      Size.fromHeight(54); // TODO: properly implement height
}

class _DefaultAppBarState extends State<DefaultAppBar> {
  @override
  PreferredSizeWidget build(BuildContext context) {
    return AppBar(
      title: DefaultAppBarTitle(),
      actions: widget.actions,
    );
  }
}

class DefaultAppBarTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) => AlignedMarkdown(
        data: '## free2**pass**',
        color: Colors.white,
      );
}
