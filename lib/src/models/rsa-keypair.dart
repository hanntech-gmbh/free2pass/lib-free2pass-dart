class RSAKeyPair {
  final String privateKey;
  final String publicKey;

  RSAKeyPair(this.privateKey, this.publicKey);
}
