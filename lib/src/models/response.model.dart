import 'error-codes.enum.dart';

/// the data model to be returned by any [ApiModel] request
class ResponseModel<T> {
  /// the [T] typed response payload from the server
  final T? payload;

  /// the [Status] of the request
  final Status status;

  ResponseModel(this.payload, this.status);

  /// JSON decode
  ResponseModel.fromJson(dynamic json)
      : payload = json['payload'] as T?,
        status = Status.fromJson(json['status']);

  @override
  String toString() {
    return '{ status: $status, payload: $payload }';
  }
}

/// the [Status] of a [ResponseModel]
class Status {
  /// whether the request was successful
  final bool success;

  /// an optional message
  final String? message;

  /// the error code of the request
  final ErrorCodes? code;

  /// any errors
  final errors;

  Status(
    this.success, [
    this.message,
    this.code,
    this.errors,
  ]);

  /// JSON decode
  Status.fromJson(dynamic json)
      : success = json['success'],
        message = json['success'] == false ? json['message'] : null,
        code = json['success'] == false ? getErrorCode(json['code']) : null,
        errors = json['success'] == false ? json['errors'] : null;

  /// JSON encode
  Map<String, dynamic> toJson() => {
        'success': success,
        'message': message,
        'code': errorCodeToInt(code),
        'errors': errors,
      };

  @override
  String toString() {
    return '{ success: $success, message: $message, code: $code, errors: $errors }';
  }
}
