/// a model containing some information on the device currently used
class DeviceInfoModel {
  final String? id;
  final String? appVersion;
  final String? appName;
  final String? deviceName;
  final String? osName;
  final String? osVersion;

  DeviceInfoModel({
    this.id,
    this.appVersion,
    this.appName,
    this.deviceName,
    this.osName,
    this.osVersion,
  });

  /// JSON encodes
  Map<String, dynamic> toJson() => {
        'id': id,
        'appVersion': appVersion,
        'appName': appName,
        'deviceName': deviceName,
        'osName': osName,
        'osVersion': osVersion
      };

  @override
  String toString() {
    return 'id: $id, appVersion: $appVersion, appName: $appName, deviceName: $deviceName, osName: $osName, osVersion: $osVersion';
  }
}
