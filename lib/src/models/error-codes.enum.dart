// TODO: deprecated

enum ErrorCodes {
  RuntimeError,
  ValidationFailed,
  CompanyNeeded,
  UnknownTable,
  UnknownExtra,
  NoMultipleOptionsForExtra,
  UnknownOrder,
  WrongVerifiyCode,
  ResetHashInvalid,
  CaptchaRequired,
  UserAlreadyExists,
  VerifyHashInvalid,
  EmailNotVerified,
  EmailAlreadyVerified,
  PasswordTooWeak,
  PaymentMethodNotFound,
  MaxSendAttemptsReached,
  PayTypeOnlyLoggedIn,
  PayTypeOnlyWithPaymentMethod,
  PaypalOnlyFrom10EUR,
  PayLaterNotForTakeAway,
  TableAlreadyExists,
  CouldNotVerifySMS,
  Foobar,
  PaymentMethodValidationFailed,
}

int errorCodeToInt(ErrorCodes? errorCode) {
  switch (errorCode) {
    case ErrorCodes.RuntimeError:
      return 0;
    case ErrorCodes.ValidationFailed:
      return 1;
    case ErrorCodes.CompanyNeeded:
      return 2;
    case ErrorCodes.UnknownTable:
      return 3;
    case ErrorCodes.UnknownExtra:
      return 4;
    case ErrorCodes.NoMultipleOptionsForExtra:
      return 5;
    case ErrorCodes.UnknownOrder:
      return 6;
    case ErrorCodes.WrongVerifiyCode:
      return 7;
    case ErrorCodes.ResetHashInvalid:
      return 8;
    case ErrorCodes.CaptchaRequired:
      return 9;
    case ErrorCodes.UserAlreadyExists:
      return 10;
    case ErrorCodes.VerifyHashInvalid:
      return 11;
    case ErrorCodes.EmailNotVerified:
      return 12;
    case ErrorCodes.EmailAlreadyVerified:
      return 13;
    case ErrorCodes.PasswordTooWeak:
      return 14;
    case ErrorCodes.PaymentMethodNotFound:
      return 15;
    case ErrorCodes.MaxSendAttemptsReached:
      return 16;
    case ErrorCodes.PayTypeOnlyLoggedIn:
      return 17;
    case ErrorCodes.PayTypeOnlyWithPaymentMethod:
      return 18;
    case ErrorCodes.PaypalOnlyFrom10EUR:
      return 19;
    case ErrorCodes.PayLaterNotForTakeAway:
      return 20;
    case ErrorCodes.TableAlreadyExists:
      return 21;
    case ErrorCodes.CouldNotVerifySMS:
      return 22;
    case ErrorCodes.Foobar:
      return 23;
    case ErrorCodes.PaymentMethodValidationFailed:
      return 24;
    default:
      return 0;
  }
}

ErrorCodes getErrorCode(int? errorCode) {
  switch (errorCode) {
    case 0:
      return ErrorCodes.RuntimeError;
    case 1:
      return ErrorCodes.ValidationFailed;
    case 2:
      return ErrorCodes.CompanyNeeded;
    case 3:
      return ErrorCodes.UnknownTable;
    case 4:
      return ErrorCodes.UnknownExtra;
    case 5:
      return ErrorCodes.NoMultipleOptionsForExtra;
    case 6:
      return ErrorCodes.UnknownOrder;
    case 7:
      return ErrorCodes.WrongVerifiyCode;
    case 8:
      return ErrorCodes.ResetHashInvalid;
    case 9:
      return ErrorCodes.CaptchaRequired;
    case 10:
      return ErrorCodes.UserAlreadyExists;
    case 11:
      return ErrorCodes.VerifyHashInvalid;
    case 12:
      return ErrorCodes.EmailNotVerified;
    case 13:
      return ErrorCodes.EmailAlreadyVerified;
    case 14:
      return ErrorCodes.PasswordTooWeak;
    case 15:
      return ErrorCodes.PaymentMethodNotFound;
    case 16:
      return ErrorCodes.MaxSendAttemptsReached;
    case 17:
      return ErrorCodes.PayTypeOnlyLoggedIn;
    case 18:
      return ErrorCodes.PayTypeOnlyWithPaymentMethod;
    case 19:
      return ErrorCodes.PaypalOnlyFrom10EUR;
    case 20:
      return ErrorCodes.PayLaterNotForTakeAway;
    case 21:
      return ErrorCodes.TableAlreadyExists;
    case 22:
      return ErrorCodes.CouldNotVerifySMS;
    case 23:
      return ErrorCodes.Foobar;
    case 24:
      return ErrorCodes.PaymentMethodValidationFailed;
  }

  return ErrorCodes.RuntimeError;
}
